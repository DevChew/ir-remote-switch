#include <EEPROM.h>

//PINS
int ledPin = 1;
int relayPin = 0;
int irPin = 2;
int buttonPin = 3;

//timing
int timeToTriggerRelay = 1500;
int buttonHoldTime = 2000;

//IR
int start_bit = 2200; //Start bit threshold (Microseconds)
int bin_1 = 1000;     //Binary 1 threshold (Microseconds)
int bin_0 = 400;      //Binary 0 threshold (Microseconds)
const byte BIT_PER_BLOCK = 32;

//EEPROM
int eepromStoreAddress = 0;


void setup() {
  pinMode(irPin, INPUT);
  pinMode(ledPin, OUTPUT);
  pinMode(relayPin, OUTPUT);

  // Set button input pin
  pinMode(buttonPin, INPUT);
  digitalWrite(buttonPin, LOW );
}

void loop() {

  if(digitalRead(buttonPin) == HIGH) {  
    delay(buttonHoldTime);
    if(digitalRead(buttonPin) == HIGH){
      digitalWrite(ledPin, HIGH);
      while(programNewButton());
      digitalWrite(ledPin, LOW);
    }
  }
  
  if(checkIfRightIRKeyIsPressed()){
    action();
  }

}
/////////////////////////////////////////////////////////////
// decode infrared signal
/////////////////////////////////////////////////////////////
int getIRKey() {

  // if (pulseIn(irPin, HIGH) >= start_bit) {
  //   return -1;
  // }

  int data[BIT_PER_BLOCK];
  int i;
  while (pulseIn(irPin, HIGH) < start_bit); //Wait for a start bit

  for (i = 0; i < BIT_PER_BLOCK; i++)
    data[i] = pulseIn(irPin, HIGH); //Start measuring bits, I only want HIGH pulses

  delay(100);
  for (i = 0; i < BIT_PER_BLOCK; i++) //Parse them
  {
    if (data[i] > bin_1) //is it a 1?
      data[i] = 1;
    else if (data[i] > bin_0) //is it a 0?
      data[i] = 0;
    else
      return -1; //Flag the data as invalid; Return -1 on invalid data
  }
  //based on NEC protocol, command data started from bit 16
  //and end with bit 24 (8 bits long)
  int result = 0;
  for (i = 16; i < 24; i++)
  {
    //DigiKeyboard.print(data[i]); //print out the value of button in binary form
    if (data[i] == 1)
      result |= (1 << i - 16); //Convert data bits to integer
  }
  return result; //Return key number
}

bool programNewButton() {
  int newKey = getIRKey();
  if (newKey > 0) {
    EEPROM.write(eepromStoreAddress, newKey);
    return true;
  } else {
    return false;
  }
}

bool checkIfRightIRKeyIsPressed() {
  int pressedKey = getIRKey();
  int storedKey = EEPROM.read(eepromStoreAddress);
  //int storedKey = 24;
  if (pressedKey == storedKey) {
    return true;
  } else {
    return false;
  }
}

void action() {
  digitalWrite(ledPin, HIGH);
  digitalWrite(relayPin, HIGH);
  delay(timeToTriggerRelay);
  digitalWrite(ledPin, LOW);
  digitalWrite(relayPin, LOW);
}
